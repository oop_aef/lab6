package com.anusit.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank anusit = new BookBank("anusit", 100.0);
        anusit.print();
        anusit.deposit(50);
        anusit.print();
        anusit.withdraw(50);
        anusit.print();

        BookBank prayood = new BookBank("prayood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000e3);
        prayood.print();

        BookBank praweet = new BookBank("praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}
